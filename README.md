## About

SPA com ReactJS com uso seguro de rotas para gestão de livros.

![](./SignIn.png)

## Passos de instalação

- git clone https://gitlab.com/gyn.carlos/book-app-front.git
- cd book-app-front
- yarn install
- yarn start

## SignIn

```shell
    email: demo@book.com
    password: demo
``` 
