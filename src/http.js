// ajax
import axios from 'axios';

const http = axios.create({
    baseURL: "http://localhost:8020",
    withCredentials: true
});

export default http;