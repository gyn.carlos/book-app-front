import { useState, useEffect } from 'react';

import {Row, Col, Card} from 'react-bootstrap';
import Swal from 'sweetalert2';
import http from "../http";

import ContentLoader from 'react-content-loader';

export default function ClimaTempo() 
{
    const [geoLocationFail, setGeoLocationFail] = useState(false);
    const [loading, setLoading] = useState();
    const [nomeCidade, setCidade] = useState();
    const [temperatura, setTemperatura] = useState();
    const [descricao, setDescricao] = useState(); 

    function getAPI() 
    {
        function requestLocation() 
        {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(getPosition, showErrorGeoLocation);
            } else {
                Swal.fire('Atenção', 'Seu navegador não suporta geolocalização', 'error');
                geoLocationFail(true);
            }
        }
    
        function showErrorGeoLocation(error)
        {
            setLoading(true);
            setGeoLocationFail(true);
        }
    
        async function getPosition(position) 
        {
            const lat = position.coords.latitude;
            const lon = position.coords.longitude;            

            try {
                await http.get("/sanctum/csrf-cookie");
                const res = await http.get(`/api/climate?lat=${lat}&lon=${lon}`);
                if (res.data.results) {
                    setCidade(res.data.results.city);
                    setTemperatura(res.data.results.temp + "°");
                    setDescricao(res.data.results.description);
                    setLoading(true);
                    
                } else {
                    setGeoLocationFail(false);
                    Swal.fire('Atenção', 'Falha ao tentar consultar as informações do clima', 'error');
                }

            } catch (err) {
                Swal.fire('Atenção', 'Falha ao tentar consultar as informações do clima', 'error');
            }
        }

        requestLocation();
    }   

    useEffect(() => {
        // atrasa um pouco o carregamento
        setTimeout(function() {
            getAPI();
        },50)
    })

    if (geoLocationFail === true) {
        return (
            <div className="mb-3 climatempo-wrapper">
                <Card className="wheater">
                <Card.Header>
                    <strong>Clima tempo</strong>
                    </Card.Header>
                    <Card.Body>              
                        <Row className="justify-content-center align-items-center">
                        <Col>
                            <small className="fs-8">Recurso desativado ou temporariamente indisponível</small>
                        </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </div>
        )
    }

    if (!loading) {
        return (
            <div className="mb-3 climatempo-wrapper">
                <ContentLoader 
                    speed={2}
                    width={400}
                    height={160}
                    viewBox="0 0 400 160"
                    backgroundColor="#a4aec6"
                    foregroundColor="#ecebeb"
                >
                    <rect x="48" y="8" rx="3" ry="3" width="88" height="6" /> 
                    <rect x="48" y="26" rx="3" ry="3" width="52" height="6" /> 
                    <rect x="0" y="56" rx="3" ry="3" width="410" height="6" /> 
                    <rect x="0" y="72" rx="3" ry="3" width="380" height="6" /> 
                    <rect x="0" y="88" rx="3" ry="3" width="178" height="6" /> 
                    <circle cx="20" cy="20" r="20" />
                </ContentLoader>               
            </div>
        )
    }

    return (
    <div className="mb-3 climatempo-wrapper">
        <Card className="wheater">
          <Card.Header>
            <strong>Clima tempo</strong>
            </Card.Header>
            <Card.Body>              
                <Row className="justify-content-center align-items-center">
                    <Col>
                    <strong className="fs-5">{nomeCidade}</strong>
                    </Col>
                    <Col className="text-center">
                        <strong className="fs-5">{temperatura}</strong>
                    </Col>
                </Row>
                <Row className="justify-content-center align-items-center">
                    <Col>
                        <small className="fs-8">{descricao}</small>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    </div>        
    )
}
