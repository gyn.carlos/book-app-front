
// utils

function storeToken(token) 
{
  window.localStorage.setItem("token", token);
}

function getToken()
{
  return window.localStorage.getItem("token");
}

export default {getToken, storeToken};
