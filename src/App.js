import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './Login.css';
// ajax
import http from "./http";
import Swal from 'sweetalert2'

import React, { useState, createContext, useContext, useEffect } from 'react';
import { BrowserRouter, Route, Switch, useHistory, useParams } from 'react-router-dom';

// icones
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faSignOutAlt, faSearch, faTrashAlt, faEdit } from "@fortawesome/free-solid-svg-icons";

import {Navbar, Container, Nav, Button, Form, FormControl} from 'react-bootstrap';
import ClimaTempo from './components/ClimaTempo';

const Context = createContext(null);

// utils

function storeToken(token) 
{
  window.localStorage.setItem("token", token);
}

function getToken()
{
  return window.localStorage.getItem("token");
}

// componentes

function Cadastro()
{
  let history = useHistory();

  let { id } = useParams();
  const [titulo, setTitulo] = useState('');
  const [descricao, setDescricao] = useState('');
  const [autor, setAutor] = useState('');
  const [pages, setPages] = useState('');
  const [dataRegistro, setDataRegistro] = useState('');

  function getHeaders()
  {
    return {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + getToken()
      }
    }
  }

  function handlerCancelar()
  {
    history.push("/")
  }

  async function getRegistro(id) 
  {
    const convertIso = (data) => {
      return (new Date(data)).toISOString().split('T')[0]
    }

    try {
        await http.get("/sanctum/csrf-cookie");
        const res = await http.get(`/api/books/${id}`, getHeaders());
        
        if (res.data) {
            setTitulo(res.data.title);
            setDescricao(res.data.description);
            setAutor(res.data.author);
            setPages(res.data.pages);
            let data = convertIso(res.data.registration_at);            
            setDataRegistro(data);
        } else {
            Swal.fire('Atenção', res.data.message, 'error');
        }
      } catch (err) {
          Swal.fire(err, '', 'error');
      }
  }

  async function handlerSalvar() 
  {
    try {

      let dataBook = {
        "title" : titulo,
        "description": descricao,
        "author": autor,
        "pages": pages,
        "registration_at": dataRegistro
      };

      const displayMessage = (res) => {
        console.log(res);
        if (res.data.success) {
          Swal.fire('Informação', res.data.message, 'success').then((result) => {
            handlerCancelar();
          })
        } else {
          Swal.fire('Atenção', res.data.message, 'error');
        }
      };

      await http.get("/sanctum/csrf-cookie");
      if (id === undefined) {
        const res = await http.post('/api/books',dataBook, getHeaders());
        displayMessage(res);
      } else {
        const res = await http.put(`/api/books/${id}`,dataBook, getHeaders());
        displayMessage(res);
      }

    } catch(Error) {
      alert('Falha ao tentar salvar os dados');
    }
  }
  
  useEffect(() => {
    if (id !== undefined) getRegistro(id);

  }, [id]);

  return (
    <Form className="bg-white p-3 rounded-3">
      <Form.Group className="mb-3">
        <Form.Control type="text" placeholder="Título" required value={titulo} onChange={e => setTitulo(e.target.value)}/>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Control type="text" placeholder="Descrição" required value={descricao} onChange={e => setDescricao(e.target.value)}/>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Control type="text" placeholder="Autor" required value={autor} onChange={e => setAutor(e.target.value)}/>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Control type="number" placeholder="Páginas" required value={pages} onChange={e => setPages(e.target.value)}/>
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Control type="date" required value={dataRegistro} onChange={e => setDataRegistro(e.target.value)}/>
      </Form.Group>
      <Button variant="primary" type="button" onClick={handlerSalvar}>
        Salvar
      </Button>
      <Button variant="secondary" type="button" className="ms-2" onClick={handlerCancelar}>
        Cancelar
      </Button>
    </Form>
  )
}

function Listagem()
{
  const ctx = useContext(Context);
  let history = useHistory();

  function getHeaders()
  {
    return {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + getToken()
      }
    }
  }  

  function editBook(bookId)
  {
    history.push(`/cadastro/${bookId}/edit`);  
  }

  function deleteBook(bookId)
  {
    if (!/[0-9]/.test(bookId)) return;

    async function removeItem (bookId) 
    {
      await http.get("/sanctum/csrf-cookie");
      const res = await http.delete(`/api/books/${bookId}`, getHeaders());
      if (res.data.success) {
          Swal.fire('Informação', res.data.message, 'success');
      } else {
          Swal.fire('Atenção', res.data.message, 'error');
      }
    }

    Swal.fire({
      title: 'Confirma a exclusão do livro?',
      icon: 'question',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
  }).then((result) => {
      if (result.isConfirmed) {
          try {
            removeItem(bookId);
            history.push("/");
          } catch (err) {
              Swal.fire(err, '', 'error');
          }
      }
  });
  }
  
  return (
  <div className="list-group">
  {
    ctx.books?.data ? 
    ctx.books?.data?.map((book) => (
            <a href="#" className="list-group-item list-group-item-action flex-column align-items-start mb-2">
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">{book?.title}</h5>              
            </div>
            <p className="mb-1">{book?.author}</p>
            <Nav className="ms-auto">
              <Button className="btn-sm me-1" variant="outline-primary" onClick={(e) => editBook(book?.id)}><FontAwesomeIcon icon={faEdit}/> Editar</Button>
              <Button className="btn-sm me-1" variant="outline-danger" onClick={(e) => deleteBook(book?.id)}><FontAwesomeIcon icon={faTrashAlt}/> Remover</Button>
            </Nav>
          </a>

      )) : ""
    }
  </div>
  )
}

function Header() 
{
  const [search, setSearch] = useState();
  
  let history = useHistory();
  
  const ctxGeral  = useContext(Context);   

  function getHeaders()
  {
    let token = getToken();

    return {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    }
  }  

  async function filtrar()
  {
    if(search !== undefined) {
      try {
      
        await http.get("/sanctum/csrf-cookie");
        const res = await http.get('/api/books?fullts=' + search, getHeaders());
        if (res.data.data && res.data.data.length) {
          ctxGeral.setBooks(res.data);
          history.push("/listagem");
        } else {
          Swal.fire('Atenção', 'Nenhum registro encontrado');
          history.push("/");
        }
  
      } catch(Error) {
        Swal.fire('Atenção','Falha ao tentar realizar consulta');
      }
    }    
  }

  function criar()
  {
    history.push("/cadastro");
  }

  async function logout() 
  {
    const destroyToken = () => 
    {
      window.localStorage.removeItem("token");
      ctxGeral.setToken("");
      history.replace("/");
    }

    try {
      
      await http.get("/sanctum/csrf-cookie");
      const res = await http.post('/api/logout', [] , getHeaders());
      if (res.data.success) {
          destroyToken();
      } else {
        Swal.fire('Atenção', res.data.message, 'error');
      }

    } catch(Error) {
      Swal.fire('Atenção','Falha ao tentar encerrar sessão');
    }
  }

  return (
    <Navbar bg="light" expand="lg" className="shadow-sm mb-3">
      <Container>
        <Navbar.Brand href="/">App Book Demo</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
        <Form className="d-flex">
          <FormControl type="search" placeholder="Search" className="mr-2" aria-label="Search" onChange={e => setSearch(e.target.value)} />
          <Button variant="outline-success" className="ms-2" onClick={filtrar}><FontAwesomeIcon icon={faSearch}/></Button>
          <Button variant="primary" className="ms-2" onClick={criar}>Novo</Button>
        </Form>
        <div className="ms-auto">Olá <strong>Demo</strong>, seja bem vindo.</div>
        <Nav className="ms-auto">
          <Button onClick={logout}><FontAwesomeIcon icon={faSignOutAlt} /> Sair</Button>
        </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>    
  )
}

function Login() 
{
  const [email,    setEmail]    = useState();
  const [password, setPassword] = useState();  
  const ctx = useContext(Context);

  function getHeaders()
  {
    return {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + getToken()
      }
    }
  }

  async function loginUser(credentials) 
  {
    try {
      
      await http.get("/sanctum/csrf-cookie");
      const res = await http.post('/api/auth',JSON.stringify(credentials), getHeaders());
      if (res.data.success) {
        return res.data.access_token;
      }
    } catch(Error) {
      return false
    }
  }
  
  async function handleSubmit(e) 
  {
    e.preventDefault();
    const token = await loginUser({email,password});
    if (token === false) {
      Swal.fire('Atenção', "Usuário ou senha inválidas", 'error');
    } else {
      ctx.setToken(token);
      storeToken(token)
    }
  }

  return (
      <div className="login-wrapper">
        <div className="bg-white p-5 rounded-3 shadow-lg box-login">
            <h1 className="font-weight-bold text-center mb-4">Sign In</h1>
            <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formGroupEmail">
              <Form.Label className="font-weight-bold">Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" onChange={e => setEmail(e.target.value)}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formGroupPassword">
              <Form.Label className="font-weight-bold">Password</Form.Label>
              <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)}/>
            </Form.Group>
            <Button variant="primary" type="submit" className="font-weight-bold w-100">Login</Button>
          </Form>
        </div>
    </div>
  )
}

function App() 
{
  // Capturando o token salvo
  const [token, setToken] = useState();
  const [books, setBooks] = useState();

  useEffect(() => {

    let tokenSaved = getToken();

    if (tokenSaved !== undefined) {
      setToken(tokenSaved)
    }
    
  }, [token, getToken]);

  if (!token) {
    return (
      <div className="wrapper">
        <Context.Provider value={{token, setToken}}>
          <Login />
        </Context.Provider>
      </div>
    );
  }

  return (
      <Context.Provider value={{token, setToken, books, setBooks}}>
        <BrowserRouter>
        <Header/>
        <div className="container">
          <Switch>
            <Route exact path="/">
            <div className="mb-3 climatempo-wrapper">
                <ClimaTempo/>
            </div>
            </Route>
            <Route path="/Listagem">
                <Listagem />
            </Route>
            <Route exatc path="/Cadastro/:id">
              <Cadastro />
            </Route>
            <Route exatc path="/Cadastro">
              <Cadastro />
            </Route>            
          </Switch>
          </div>
        </BrowserRouter>        
      </Context.Provider>    
  )
}

export default App;
